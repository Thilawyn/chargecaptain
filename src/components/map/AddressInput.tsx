import { Close, Colorize, Search } from "@mui/icons-material"
import { IconButton, Stack, TextField, Tooltip } from "@mui/material"
import { Map } from "leaflet"
import React, { useEffect } from "react"
import { AddressState, useMapState } from "./MapProvider"


export default function AddressInput(
    { state, label, loading }:
        {
            state: string
            label: string
            loading: boolean
        }
) {

    const [mapState, setMapState] = useMapState()
    const addressState: AddressState = (mapState as any)[ state ]


    const coordsPickMode = mapState.mode === state

    useEffect(() => {
        if (!coordsPickMode) return

        const { map } = mapState

        function onClick({ latlng: { lat, lng } }: any) {
            setMapState({
                ...mapState,

                mode: undefined,
                [ state ]: {
                    address: undefined,
                    coords: [lat, lng],
                },
            })
        }
        map?.on("click", onClick)

        return () => {
            map?.off("click", onClick)
        }

    }, [mapState])


    return (
        <Stack
            direction="row"
            alignItems="center"

            spacing={1}
        >
            <TextField
                variant="filled"
                size="small"
                label={label}
                disabled={loading}

                value={addressState.address || addressState.coords?.toString() || ""}
                onChange={e => {
                    setMapState({
                        ...mapState,

                        [ state ]: {
                            address: e.target.value,
                            coords: undefined,
                        },
                    })
                }}
            />

            {/* <Tooltip title="Chercher l'adresse entrée">
                <IconButton
                    onClick={() => {
                    }}
                >
                    <Search />
                </IconButton>
            </Tooltip> */}

            <Tooltip title={coordsPickMode ? "Annuler" : "Sélectionner un point sur la carte"}>
                <span>
                    <IconButton
                        disabled={loading}
                        onClick={() => {
                            setMapState({
                                ...mapState,
                                mode: coordsPickMode ? undefined : state,
                            })
                        }}
                    >
                        {coordsPickMode ? <Close /> : <Colorize /> }
                    </IconButton>
                </span>
            </Tooltip>
        </Stack>
    )

}
