import L from "leaflet"
import "leaflet/dist/leaflet.css"
import React, { useEffect } from "react"
import { MapContainer, Marker, Polyline, TileLayer, Tooltip } from "react-leaflet"
import { useNavigate } from "react-router-dom"
import { useCar } from "../cars/CarProvider"
import Directions from "./Directions"
import Itinerary from "./Itinerary"
import { useMapState } from "./MapProvider"


const icon = L.icon({
    iconUrl: "/static/marker-icon.png",
    iconSize: [50, 82],
    iconAnchor: [25, 82],
})


export default function MapPage() {

    const navigate = useNavigate()
    const [car, setCar] = useCar()

    useEffect(() => {
        if (!car) navigate("/")
    }, [car])


    const [mapState, setMapState] = useMapState()

    useEffect(() => {
        if (!mapState.route) return

        const bbox = mapState.route.bbox

        mapState.map?.fitBounds([
            [ bbox[1], bbox[0] ],
            [ bbox[3], bbox[2] ],
        ])

    }, [mapState.route])


    return <>
        <MapContainer
            center={[46.804, 3.021]}
            zoom={7}
            zoomControl={false}

            whenCreated={map => setMapState({ ...mapState, map })}
        >
            <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />

            {mapState.start.coords &&
                <Marker
                    position={mapState.start.coords}
                    icon={icon}
                >
                    <Tooltip sticky>Départ</Tooltip>
                </Marker>
            }

            {mapState.end.coords &&
                <Marker
                    position={mapState.end.coords}
                    icon={icon}
                >
                    <Tooltip sticky>Arrivée</Tooltip>
                </Marker>
            }

            {mapState.route &&
                <Polyline
                    positions={mapState.route.geometry.coordinates}
                />
            }
        </MapContainer>

        {mapState.map &&
            <Itinerary />
        }

        {mapState.route &&
            <Directions />
        }
    </>

}
