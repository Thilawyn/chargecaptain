import { PlayCircleOutline } from "@mui/icons-material"
import { IconButton, Paper, Stack, Tooltip } from "@mui/material"
import axios from "axios"
import { useSnackbar } from "notistack"
import React, { useState } from "react"
import { useCar } from "../cars/CarProvider"
import AddressInput from "./AddressInput"
import { useMapState } from "./MapProvider"


export default function Itinerary() {

    const { enqueueSnackbar } = useSnackbar()

    const [car, setCar] = useCar()

    const [mapState, setMapState] = useMapState()
    const canProcessRoute = mapState.start.coords && mapState.end.coords

    const [loading, setLoading] = useState(false)

    async function fetchRoute() {
        setLoading(true)

        try {
            const { data } = await axios.post("/api/route/between", {
                car,
                start: mapState.start.coords,
                end: mapState.end.coords,
            })

            setMapState({ ...mapState, route: data })
        }
        catch(e) {
            console.error(e)
            enqueueSnackbar("Impossible de trouver un chemin entre les deux points !", { variant: "error" })
        }

        setLoading(false)
    }


    return (
        <Paper
            elevation={4}
            sx={{
                zIndex: 500,
                position: "absolute",
                top: 20,
                left: 20,

                borderRadius: 10,
                padding: 2,
            }}
        >
            <Stack
                direction="row"
                alignItems="center"

                spacing={2}
            >
                <Stack
                    direction="column"
                    alignItems="flex-start"
                >
                    <AddressInput
                        state="start"
                        label="Départ"
                        loading={loading}
                    />

                    <AddressInput
                        state="end"
                        label="Destination"
                        loading={loading}
                    />
                </Stack>

                <Tooltip title={canProcessRoute ? "Calculer le trajet" : "Veuillez d'abord sélectionner un point de départ et d'arrivée"}>
                    <span>
                        <IconButton
                            size="large"
                            disabled={!canProcessRoute || loading}
                            onClick={fetchRoute}
                        >
                            <PlayCircleOutline />
                        </IconButton>
                    </span>
                </Tooltip>
            </Stack>
        </Paper>
    )

}
