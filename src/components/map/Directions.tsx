import { Close } from "@mui/icons-material"
import { IconButton, Paper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Tooltip, Typography } from "@mui/material"
import formatDuration from "format-duration"
import { reduce } from "lodash"
import React, { Fragment, useEffect, useState } from "react"
import { useMapState } from "./MapProvider"


export default function Directions() {

    const [mapState, setMapState] = useMapState()

    const [totalDistance, setTotalDistance] = useState(0)
    const [totalDuration, setTotalDuration] = useState(0)

    useEffect(() => {
        setTotalDistance(
            reduce(
                mapState.route.properties.segments,
                (acc, segment) => acc + segment.distance,
                0,
            )
        )

        setTotalDuration(
            reduce(
                mapState.route.properties.segments,
                (acc, segment) => acc + segment.duration,
                0,
            )
        )
    }, [mapState.route])


    return (
        <Paper
            elevation={4}
            sx={{
                zIndex: 500,
                position: "absolute",
                top: "50%",
                left: 20,
                right: 20,
                bottom: 20,

                borderRadius: 10,
                padding: 2,

                overflow: "auto",
            }}
        >
            <Stack
                direction="row"
                justifyContent="space-between"
                alignItems="center"

                sx={{
                    marginBottom: 2,
                }}
            >
                <div>
                    <Typography>Distance : {(totalDistance / 1000).toFixed(2)} km</Typography>
                    <Typography>Durée : {formatDuration(totalDuration * 1000)}</Typography>
                </div>

                <Tooltip title="Quitter la navigation">
                    <IconButton
                        size="large"
                        onClick={() => setMapState({ ...mapState, route: undefined })}
                    >
                        <Close />
                    </IconButton>
                </Tooltip>
            </Stack>

            {mapState.route.properties.segments.map((segment: any, index: number) =>
                <Fragment key={index}>
                    <TableContainer component={Paper}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell align="left">Distance</TableCell>
                                    <TableCell align="left">Durée</TableCell>
                                    <TableCell align="left">Voie</TableCell>
                                    <TableCell align="left">Instruction</TableCell>
                                </TableRow>
                            </TableHead>

                            <TableBody>
                                {segment.steps.map((step: any, index: number) =>
                                    <TableRow key={index}>
                                        <TableCell align="left">{step.distance} m</TableCell>
                                        <TableCell align="left">{formatDuration(step.duration * 1000)}</TableCell>
                                        <TableCell align="left">{step.name}</TableCell>
                                        <TableCell align="left">{step.instruction}</TableCell>
                                    </TableRow>
                                )}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Fragment>
            )}
        </Paper>
    )

}
