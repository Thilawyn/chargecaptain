import { Map } from "leaflet"
import React, { createContext, ReactNode, useContext, useState } from "react"


export type MapState = {
    map?: Map
    mode?: string

    start: AddressState
    end: AddressState
    route: any
}

export type AddressState = {
    address?: string
    coords?: [number, number]
}


const MapContext = createContext<[
    MapState,
    React.Dispatch< React.SetStateAction<MapState> >,
]>([
    {
        start: {},
        end: {},
        route: null,
    },

    () => {},
])


export function useMapState() {
    return useContext(MapContext)
}


export default function MapProvider(
    { children }: { children: ReactNode }
) {

    const mapState = useState<MapState>({
        start: {},
        end: {},
        route: null,
    })


    return (
        <MapContext.Provider value={mapState}>
            { children }
        </MapContext.Provider>
    )

}
