import { createTheme, CssBaseline, ThemeProvider, Typography } from "@mui/material"
import { SnackbarProvider } from "notistack"
import React from "react"
import { BrowserRouter, Route, Routes } from "react-router-dom"
import CarProvider from "./cars/CarProvider"
import CarSelectionPage from "./cars/CarSelectionPage"
import MapPage from "./map/MapPage"
import MapProvider from "./map/MapProvider"


const theme = createTheme({
    components: {
        MuiCssBaseline: {
            styleOverrides: {
                body: {
                },
            },
        },
    },
})


export default function App() {

    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />

            <SnackbarProvider>
                <CarProvider>
                    <MapProvider>
                        <BrowserRouter>
                            <Routes>
                                <Route
                                    path="/"
                                    element={<CarSelectionPage />}
                                />

                                <Route
                                    path="/map"
                                    element={<MapPage />}
                                />

                                <Route
                                    path="*"
                                    element={<Typography variant="h5" align="center">404</Typography>}
                                />
                            </Routes>
                        </BrowserRouter>
                    </MapProvider>
                </CarProvider>
            </SnackbarProvider>
        </ThemeProvider>
    )

}
