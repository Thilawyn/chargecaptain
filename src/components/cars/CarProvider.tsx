import React, { createContext, ReactNode, useContext, useState } from "react"


const CarContext = createContext<[
    any,
    React.Dispatch< React.SetStateAction<any> >,
]>([
    null,
    () => {},
])


export function useCar() {
    return useContext(CarContext)
}


export default function CarProvider(
    { children }: { children: ReactNode }
) {

    const carState = useState(null)


    return (
        <CarContext.Provider value={carState}>
            { children }
        </CarContext.Provider>
    )

}
