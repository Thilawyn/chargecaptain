import { Button, Container, LinearProgress, List, ListItem, Stack, Typography } from "@mui/material"
import axios from "axios"
import { useSnackbar } from "notistack"
import React, { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import { Car } from "../../soap/cars"
import { useCar } from "./CarProvider"
import formatDuration from "format-duration"


export default function CarSelectionPage() {

    const { enqueueSnackbar } = useSnackbar()
    const navigate = useNavigate()

    const [car, setCar] = useCar()

    useEffect(() => {
        if (car) navigate("/map")
    }, [car])


    const [loading, setLoading] = useState(true)
    const [cars, setCars] = useState<Car[] | null>(null)

    async function fetchCars() {
        setLoading(true)

        try {
            setCars( (await axios.get("/api/cars/all")).data )
        }
        catch(e) {
            console.error(e)
            enqueueSnackbar("Impossible de charger les voitures !", { variant: "error" })
        }

        setLoading(false)
    }

    useEffect(() => {
        fetchCars()
    }, [])


    return (
        <Container>
            <Typography variant="h5" align="center">Choisissez votre modèle de voiture</Typography>

            {loading ?
                <LinearProgress />
            :
                cars &&
                    <Stack
                        direction="column"
                        alignItems="center"
                        spacing={2}

                        sx={{
                            margin: 5,
                        }}
                    >
                        {cars.map((car, index) =>
                            <Stack
                                key={index}
                                direction="row"
                                alignItems="center"
                                spacing={6}

                                sx={{
                                    width: "fit-content",
                                }}
                            >
                                <div>
                                    <Typography>{car.name}</Typography>
                                    <Typography variant="body2">Prix : {car.price} €</Typography>
                                    <Typography variant="body2">Autonomie : {car.autonomy} km</Typography>
                                    <Typography variant="body2">Temps de charge rapide : {formatDuration(car.fast * 60 * 1000)}</Typography>
                                    <Typography variant="body2">Temps de charge lent : {formatDuration(car.slow * 60 * 1000)}</Typography>
                                </div>

                                <Button
                                    variant="contained"
                                    onClick={() => {
                                        setCar(car)
                                    }}
                                >
                                    Sélectionner
                                </Button>
                            </Stack>
                        )}
                    </Stack>
            }
        </Container>
    )

}
