require("dotenv").config()
import express from "express"
import path from "path"
import api from "./api/api"
import { serviceObject, wsdl } from "./soap/cars"
const soap = require("soap")


const app = express()
const httpPort = Number(process.env.PORT)


async function registerRoutes() {
    app.use("/static", express.static("static"))
    app.use("/api", api)
    soap.listen(app, "/wsdl", serviceObject, wsdl)

    if (process.env.MODE === "development") {
        const [
            { default: webpackConfig },
            { default: webpack },
            { default: webpackDevMiddleware },
            { default: webpackHotMiddleware },
        ] = await Promise.all([
            // @ts-ignore
            import("../webpack.config"),
            import("webpack"),
            import("webpack-dev-middleware"),
            import("webpack-hot-middleware"),
        ])

        const webpackCompiler = webpack(webpackConfig)

        app.use(webpackDevMiddleware(webpackCompiler, {}))
        app.use(webpackHotMiddleware(webpackCompiler, {}))

        app.get("*", (_, res) => {
            res.sendFile( path.join(process.cwd(), "index.html") )
        })
    }

    if (process.env.MODE === "production") {
        app.get("/webapp.js", (_, res) => {
            res.sendFile( path.join(process.cwd(), "dist/webapp.js") )
        })
    }

    app.get("*", (_, res) => {
        res.sendFile( path.join(process.cwd(), "index.html") )
    })
}


registerRoutes().then(() => {
    app.listen(httpPort, () => {
        console.log("HTTP listening on port " + httpPort + ".")
    })
})
