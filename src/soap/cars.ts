import path from "path";
var fs = require('fs');


export type Car = {
  name: string
  price: number
  autonomy: number
  slow: number
  fast: number
  pathImage: string
}

export const voitures = [
    {
      "name": "Aiway U5",
      "price":39700,
      "autonomy":385,
      "slow":600,
      "fast":35,
      "pathImage": "../lesVoituresEclatax/1/photo.png"
    },
    {
      "name":"Audi e-tron 50 quattron",
      "price":71900,
      "autonomy":339,
      "slow":310,
      "fast":37,
      "pathImage": "../lesVoituresEclatax/2/photo.png"
    },
    {
      "name":"Sportback 55 quattro",
      "price":86800,
      "autonomy":446,
      "slow":380,
      "fast":46,
      "pathImage": "../lesVoituresEclatax/3/photo.png"
    },
    {
      "name":"RS e-tron GT",
      "price":140700,
      "autonomy":472,
      "slow":400,
      "fast":34,
      "pathImage": "../lesVoituresEclatax/4/photo.png"
    },
    {
      "name":"Audi Q4 e-tron 35",
      "price":42800,
      "autonomy":345,
      "slow":610,
      "fast":23,
      "pathImage": "../lesVoituresEclatax/5/photo.png"
    },
    {
      "name": "Audi Q4 e-tron Sportback 35 ",
      "price":44800,
      "autonomy":345,
      "slow":370,
      "fast":23,
      "pathImage": "../lesVoituresEclatax/6/photo.png"
    },
    {
      "name":"BMW i3",
      "price":39950,
      "autonomy":310,
      "slow":166,
      "fast":40,
      "pathImage": "../lesVoituresEclatax/7/photo.png"
    },
    {
      "name":"BMW i4 eDrive 40",
      "price":59700,
      "autonomy":590,
      "slow":500,
      "fast":39,
      "pathImage": "../lesVoituresEclatax/8/photo.png"
    },
    {
      "name":"BMW iX",
      "price":86250,
      "autonomy":600,
      "slow":580,
      "fast":40,
      "pathImage": "../lesVoituresEclatax/9/photo.png"
    },
    {
      "name":"BMW iX3",
      "price":69950,
      "autonomy":460,
      "slow":325,
      "fast":39,
      "pathImage": "../lesVoituresEclatax/10/photo.png"
    },
];


// the splitter function, used by the service
function splitter_function(args: any) {
    var splitter = args.splitter;
    var message = args.message;
    var result = [];
    if (message==="all") {
      for(var i=0; i<voitures.length; i++){
        result.push(voitures[i]);
      }
    }else {
      result.push(voitures[splitter]);
      }

    return {
        result: result
        }
}

// the service
export const serviceObject = {
  MessageSplitterService: {
        MessageSplitterServiceSoapPort: {
            MessageSplitter: splitter_function
        },
        MessageSplitterServiceSoap12Port: {
            MessageSplitter: splitter_function
        }
    }
};

// load the WSDL file
export const wsdl = fs.readFileSync(path.join(process.cwd(), 'service.wsdl'), 'utf8');
