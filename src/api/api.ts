import { json, Router } from "express"
import carsApi from "./cars"
import routeApi from "./route"


const api = Router()

api.use(json())
api.use("/cars", carsApi)
api.use("/route", routeApi)

api.all("*", (_, res) => {
    res.sendStatus(404)
})


export default api
