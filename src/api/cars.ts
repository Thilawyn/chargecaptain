import { Router } from "express"
import { createClientAsync } from "soap"
import { voitures } from "../soap/cars"


const carsApi = Router()

// carsApi.get("/all", async (_, res) => {
//     try {
//         const client = await createClientAsync("http://localhost:8080/wsdl?wsdl")
//         const res = await client.MessageSplitter({
//             message: "all",
//             splitter: 0,
//         })

//         console.log(res)
//     }
//     catch(e) {
//         console.error(e)
//     }

//     res.sendStatus(200)
// })


carsApi.get("/all", (_, res) => {
    res.json(voitures)
})


export default carsApi
