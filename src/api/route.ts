import { along, distance, Feature, GeoJSONObject, length, lineString, Position } from "@turf/turf"
import axios from "axios"
import { Router } from "express"
import { map, reverse } from "lodash"
// @ts-ignore
import openrouteservice from "openrouteservice-js"
import { Car } from "../soap/cars"


const api_key = process.env.ORS_API_KEY

const directions = new openrouteservice.Directions({ api_key })
const geocode = new openrouteservice.Geocode({ api_key })


function reverseLatlng(positions: Position[]) {
    return map(positions, position => reverse(position))
}


async function getDirections(coordinates: Position[]): Promise<Feature<GeoJSONObject & { coordinates: Position[] }>> {
    const directionsRes = await directions.calculate({
        coordinates,
        profile: "driving-hgv",
        format: "geojson",
    })

    return directionsRes.features[0]
}

async function getBornesAround([ lng, lat ]: Position, radius: number) {
    return (await axios.get(
        "https://opendata.reseaux-energies.fr/api/records/1.0/search/",

        {
            params: {
                dataset: "bornes-irve",
                "geofilter.distance": lat + "," + lng + "," + radius * 1000,
            },
        },
    )).data.records
}

async function getClosestBorneAround(coords: Position, radius: number) {
    const bornes = await getBornesAround(coords, radius)

    let closestBorne: any = null

    bornes.forEach((borne: any) => {
        if (
            !closestBorne ||
            distance(borne.geometry.coordinates, coords, { units: "kilometers" }) < distance(closestBorne.geometry.coordinates, coords, { units: "kilometers" })
        )
            closestBorne = borne
    })

    return closestBorne
}

async function getDirectionsWithBornes(car: Car, start: Position, end: Position) {
    const line = await getDirections([ start, end ])
    const it = Math.floor( length(line, { units: "kilometers" }) / car.autonomy )

    let points = [ start ]

    for (let i = 1; i <= it; i++) {
        const point = await getClosestBorneAround(
            along( lineString(line.geometry.coordinates), i * car.autonomy, { units: "kilometers" } ).geometry.coordinates,
            50,
        )

        if (!point) throw "Cannot find borne"

        points = [ ...points, point.geometry.coordinates ]
    }

    return await getDirections([ ...points, end ])
}


const routeApi = Router()

routeApi.get("/reverse-geocode/:lat-:lng", async (req, res) => {
    const { lat, lng } = req.params

    try {
        res.json(
            await geocode.reverseGeocode({ point: { lat_lng: [parseFloat(lat), parseFloat(lng)] } })
        )
    }
    catch(e) {
        console.error(e)
        res.sendStatus(500)
    }
})

routeApi.post("/between", async (req, res) => {

    const { car, start, end }: {
        car: Car
        start: Position
        end: Position
    } = req.body

    try {
        const directions = await getDirectionsWithBornes(car, reverse(start), reverse(end))
        directions.geometry.coordinates = reverseLatlng(directions.geometry.coordinates)

        res.json(directions)
    }
    catch(e) {
        console.error(e)
        res.sendStatus(500)
    }

})


export default routeApi
